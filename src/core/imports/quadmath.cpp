#include <El-lite.hpp>
#ifdef EL_HAVE_QUAD

namespace El {

std::ostream& operator<<( std::ostream& os, const Quad& alpha )
{
    char str[128];
    quadmath_snprintf( str, 128, "%Qe", alpha );
    os << str;
    return os;
}

std::istream& operator>>( std::istream& is, Quad& alpha )
{
    string token;
    is >> token; 
    alpha = strtoflt128( token.c_str(), NULL );
    return is;
}

} // namespace El

#endif //ifdef EL_HAVE_QUAD
