#ifndef EL_IMPORTS_QUADMATH_HPP
#define EL_IMPORTS_QUADMATH_HPP

#ifdef EL_HAVE_QUAD
#include <quadmath.h>

namespace El {

std::ostream& operator<<( std::ostream& os, const Quad& alpha );
std::istream& operator>>( std::istream& is,       Quad& alpha );

} // namespace El
#endif // E_HAVE_QUAD

#endif // EL_IMPORTS_QUADMATH_HPP
